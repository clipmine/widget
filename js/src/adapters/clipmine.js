/**
 * Created by rroopan on 11/7/13.
 */

var clipmineAdapter = function () {
    var widget = null;
    var player = null;

    function getPlayer () {
        return player;
    }

    //public interface
    return {
        name: 'ClipMine',
        userid: -1,
        siteId: 'clipmine',
        hostname: 'localhost',
	    insertAfterNode: '#player',
        timeLineWidth: 570,
        playerWidth: 640,
        widgetClass: 'youtube-widget',
        videoUrl: null,
        embedCode: null,

        getSiteVideoIdInfo: function () {
            return {
		        site: 'clipmine',
	            video_id : widgetOptions.videoId
            }
        },

        playerReady: function () {
            if (widget == null) {
                widget = new ClipMineWidget('#player', clipmineAdapter, widgetOptions);

                this.videoUrl = widgetOptions.videoUrl;
                this.videoId = widgetOptions.videoId;
                this.embedCode = getPlayer().getVideoEmbedCode();
                widget.ready();
            }
        },

        playerStateChange: function (event) {
            if (event.data == YT.PlayerState.PLAYING) {
                console.log('Player is playing');
                widget.playing();
            }
            /* Log the new state of the player */
//            if(newState===0)
//                console.log("ENDED");
//            else if(newState===1) {
//                console.log("PLAYING");
//            }
//            else if(newState===2)
//                console.log("PAUSED");
//            else
//                console.log("SOME OTHER STATE: " + newState);
        },

        getPlayerWidth: function () {
            return 640;
        },

        getDuration: function () {
            return getPlayer().getDuration();
        },

        playVideo: function () {
            getPlayer().playVideo();
        },

        stopVideo: function () {
            getPlayer().pauseVideo();
        },

        seekTo: function (time) {
            getPlayer().seekTo(time);
        },

        getCurrentPlayerTime: function () {
            return getPlayer().getCurrentTime();
        },

        setPlayer: function (p) {
            player = p;
        },

        init: function () {
        }
    }
}();