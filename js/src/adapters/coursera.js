var courseraAdapter = function () {
    var widget = null;
    var player = null;

    function createDialog () {
        $("<div id='clipmine-video-dialog'></div>").appendTo($('body'));
        $( "#clipmine-video-dialog" ).dialog({
            dialogClass: "coursera-dialog",
            autoOpen: false,
            width: 660,
            modal: true,
            resizable: false,
            open: function () {
            },
            close: function() {
                player.pause();
                widget.clear();
                player.src = '';
            }
        });
        
        $("#clipmine-video-dialog").html('<video id="clipmine-video-player" style="overflow: hidden; z-index:0; margin-top: -35px;" height="420" width="620" controls preload><source height="420" src="" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'"></video>');
        player = $('#clipmine-video-player').get(0);//document.getElementById('clipmine-video-player').get(0);
        widget = new ClipMineWidget( "#clipmine-video-dialog" , courseraAdapter);

        player.addEventListener('loadeddata', function() {
            widget.ready();
        }, false);
    }

    return {
        videoUrl: null,
        userId: null,
        name: 'Coursera',
        siteId: 'coursera',
        hostname: 'class.coursera.org',
        playerWidth: 620,
        widgetClass: 'coursera-widget',
        timeLineWidth: 550,
        embedCode: null,

        getSiteVideoIdInfo: function () {
            console.log(getPlayer().getVideoUrl().trim());
            return {
		        site: 'coursera',
	            video_id : this.videoUrl
            }
        },

        getDuration: function () {
            return player.duration;
        },

        playVideo: function () {
            player.play();
        },

        stopVideo: function () {
            player.pause();
        },

        seekTo: function (time) {
            player.currentTime = time;
        },

        getCurrentPlayerTime: function () {
            return player.currentTime;
        },

        loadVideo: function (link) {
            $("#clipmine-video-dialog").dialog('open');
            console.log('Loading new video');
            this.videoUrl = link;
            player.src = link;
            player.load();
        },
        
        init: function () {
            createDialog();

            //find user id
            this.userId = parseInt($('a[data-user-id]').attr('data-user-id'));
            console.log('Coursera userId is ' + this.userId);

            //search for video lectures
            var videoRows = $('.course-item-list-section-list li');
            console.log('Found ' + videoRows.length + ' video rows.');
            for (var i = videoRows.length-1; i >= 0; i--) {
                var videoLink = $(videoRows[i]).children('a:first');
                $(videoLink).unbind('click');
                var videoMp4 = $(videoRows[i]).children('div').children('a:last').attr('href');

                var newVideoLink = $("<a href='javascript:void(0)' video-link='" + videoMp4 + "' class='lecture-link'>" + $(videoLink).text() + "</a>");
                $(videoLink).replaceWith(newVideoLink);
                var self = this;
                newVideoLink.click(function () {
                    var mp4Link = $(this).attr('video-link');
                    self.loadVideo(mp4Link);
                })
            }
        }
    }
}();