var unknownSiteAdapter = function () {
    var embedTypes = [];
    var widgets = [];
    
    //video embed types - HTML tag that can hold a video such as object, embed, iframe
	var EmbedType = function (selector) {
		this.selector = selector;
	}

	EmbedType.prototype.VideoSources = ['http://s.ytimg.com', 'youtube.com/v/', 'http://youtu.be/', 'youtube.com/embed/'];	//will need to switch to regex
	EmbedType.prototype.isValidSource = function(src) {
		for (var i = 0; i < this.VideoSources.length; i++) {
			if (src.indexOf(this.VideoSources[i]) > -1)
				return true;
		}
		return false;
	}

	embedTypes.push(new EmbedType("object[type='application/x-shockwave-flash']"));
	embedTypes.push(new EmbedType("embed[type='application/x-shockwave-flash']"));
	embedTypes.push(new EmbedType("iframe"));

    //go through various video element types and tries to detect if there is a youtube video associated with it
	function findVideosAndInsertWidget () {
		for (var x = 0; x < embedTypes.length; x++) {
			var embedType = embedTypes[x];
			var elements = $(embedType.selector);
			for (var i = 0; i < elements.length; i++) {
				var el = elements[i];
				if (embedType.isValidSource($(el).attr('src'))) {
                    var w = new ClipMineWidget(el, unknownSiteAdapter);
                    w.ready();
					widgets.push(w);
				}
			}
		}
	}

    return {
        init: function () {
            findVideosAndInsertWidget();
        }
    }
}();
