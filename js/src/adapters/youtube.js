
var youtubeAdapter = function () {
    var isHtml5 = false;
    var widget = null;
    const YOUTUBE_PLAYER_ID = 'movie_player';

    function getPlayer () {
        var ytplayer = document.getElementById('movie_player');
        if(ytplayer ===  null)
            console.log("ytplayer is null");

        return ytplayer;
    }

    function playerStateChange (newState) {
        /* Log the new state of the player */
        if(newState===0)
            console.log("ENDED");
        else if(newState===1) {
            console.log("PLAYING");
        }
        else if(newState===2)
            console.log("PAUSED");
        else
            console.log("SOME OTHER STATE: " + newState);
    }

    function embedExternalInterface () {
        var embScript = document.createElement('script');
        embScript.src = chrome.extension.getURL("widget/js/src/adapters/youtube_ext_interface.js");
        embScript.onload = function() {
            this.parentNode.removeChild(this);
        };
        (document.head||document.documentElement).appendChild(embScript);
    }

    //public interface
    return {
        name: 'YouTube',
        userid: -1,
        siteId: 'youtube',
        hostname: 'www.youtube.com',
	    insertAfterNode: '#player',
        timeLineWidth: 570,
        playerWidth: 640,
        widgetClass: 'youtube-widget',
        videoUrl: null,
        embedCode: null,

        getSiteVideoIdInfo: function () {
            console.log(getPlayer().getVideoUrl().trim());
            return {
		        site: 'youtube',
	            video_id : getPlayer().getVideoUrl().trim()
            }
        },

        playerReady: function () {
            if (widget == null) {
                widget = new ClipMineWidget( "#player" , youtubeAdapter, widgetOptions);
                this.videoUrl = getPlayer().getVideoUrl().trim();
                this.embedCode = getPlayer().getVideoEmbedCode();
                widget.ready();
            }

            getPlayer().addEventListener("onStateChange", "onYouTubePlayerStateChange");
        },

        initMessageListenter: function  () {
            var port = chrome.runtime.connect();
            var self = this;
            window.addEventListener("message", function(event) {
                // We only accept messages from ourselves
                if (event.source != window)
                    return;

                if (event.data.type && (event.data.type == "FROM_PAGE")) {
                    console.log("Content script received: " + event.data.text);
                    switch (event.data.text) {
                        case 'PLAYER_READY': self.playerReady();
                                             break;
                        case 'PLAYER_STATE_CHANGE': playerStateChange(event.data.state);
                                                    break;
                        default: console.log('Invalid text');
                    }
                }
            }, false);
        },

        getPlayerWidth: function () {
            return 640;
        },

        getDuration: function () {
            return getPlayer().getDuration();
        },

        playVideo: function () {
            getPlayer().playVideo();
        },

        stopVideo: function () {
            getPlayer().pauseVideo();
        },

        seekTo: function (time) {
            getPlayer().seekTo(time);
        },

        getCurrentPlayerTime: function () {
            return getPlayer().getCurrentTime();
        },

        init: function () {
            embedExternalInterface();
            this.initMessageListenter();
        }
    }
}();