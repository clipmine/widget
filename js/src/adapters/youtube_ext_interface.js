/*
 * This function will be called after the YouTube player is loaded and ready to receive
 * API calls. Note that this will NOT be called again when the user views a subsequent
 * video on the same page.
 */
function onYouTubePlayerReady(playerId) {
    window.postMessage({ type: "FROM_PAGE", text: "PLAYER_READY" }, "*");
}

function onYouTubePlayerStateChange(newState) {
    window.postMessage({ type: "FROM_PAGE", text: "PLAYER_STATE_CHANGE", state: newState }, "*");
}