/**
 * Created by PyCharm.
 * User: rajiv
 * Date: 10/29/13
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
const widgetContainerDiv = '#clipmine-widget-container';
const annotationDetailDiv = '#clipmine-annotation-detail';
const annotationFullDiv = '#clipmine-annotation-full';

function AnnotationView (a, widget, videoDuration, timeLineWidth) {
    this.id = null;
    this.annotation = a;
    this.tempBottomDisplay = true;
    this.el = null;
    this.widget = widget;
    this.labelEl = null;

    this.voteUp = function () {
        console.log('Voting up for annotation with id ' + this.id);
        this.annotation.votesUp++;
        this.save();
    }

    this.voteDown = function () {
        console.log('Voting down for annotation with id ' + this.id);
        this.model.votesDown++;
        this.save();
    }

    this.save = function () {
        clipMineService.saveAnnotation(this.annotation)
    }

    this.getModelValue = function (field) {
        return this.annotation[field];
    }

    this.get = function (field) {
        return this.annotation[field];
    }

    this.show = function () {
        this.el.css('display', 'block');
        // && this.get('tempBottomDisplay') == true
        console.log(this.getModelValue('tempDisplayOn'));
        if (this.isTag() && this.getModelValue('tempDisplayOn'))
            this.labelEl.css('display', 'inline-block');
    }

    this.hide = function () {
        this.el.css('display', 'none');
        // && this.get('tempBottomDisplay') == true
        if (this.isTag())
            this.labelEl.css('display', 'none');
    }

    this.showLabel = function (left, top) {
        this.labelEl.css('left', left + 'px');
        this.labelEl.css('top', top);
        this.labelEl.appendTo($(annotationDetailDiv));
    }

    this.render = function (position) {
        if (!this.isTag()) {    //comments
            this.el = $('<img>', {
                width: '20px',
                height: '20px',
                src: chrome.extension.getURL("comment-icon.png"),   //TODO: fix issue here
                class: 'annotation-comment'
            }).css('left', position + 'px');
        }
        else {
            this.el = $("<div id='marker-" + this.id + "' class='marker' start-time='" + this.startTime + "' style='left:" + position + "px;'></div>");
        }

        this.el.appendTo($(annotationFullDiv));

        var self = this;
        this.el.click(function () {
            self.widget.adapter.seekTo(self.getModelValue('startTime'));
        });

        this.el.popover({
            placement: 'bottom',
            trigger: 'hover',
            container: 'body',
            html: true,
            title: function () {
                var time = Math.round(self.getModelValue('startTime')) + '';
                if (time > 60) {
                    var secs = Math.round(time % 60);
                    time = Math.round(time / 60) + ':' +  (secs < 10 ? '0' + secs : secs);
                }

                var html = "";
                html += "<small>" + time + "s</small>";
                html += "<span style='margin-left: 5px;'>" + self.getModelValue('name') + "</span>";
//                html += "<span class='glyphicon glyphicon-trash' style='float:right;'></span>";
                html += "<span class='thumbs glyphicon glyphicon-thumbs-down' dir='down' id='" + self.id + "' style='float:right; cursor:pointer; margin-right: 7px;'></span>";
                html += "<span class='thumbs glyphicon glyphicon-thumbs-up' dir='up' id='" + self.id + "' style='float:right; cursor:pointer; margin-right: 7px;'></span>"

                return html;
            },
            content: function () {
                var html = "";
                html += "<span class='clipmine-share-annotation' id='" + self.getModelValue('id') + "'>share on Facebook</span>";
                return html;
            }
        });

        if (this.isTag()) {
            this.renderLabel();
            this.showLabel(widget.totalTopSoFar, '17px');
            widget.totalTopSoFar += this.labelEl.width() + 20;
        }
    }

    this.renderLabel = function () {
        var self = this;
        this.labelEl = $('<div style="font-size: 9pt; display:none;" class="annotation-label label">' + this.getModelValue('name') +
            ' <span class="annotation-remove" annotation-id="' + self.id + '" style="float: right;">x</span></div>');
//        this.labelEl.appendTo($(annotationDetailDiv));

        //tag popup
        this.labelEl.popover({
            placement: 'bottom',
            trigger: 'hover',
            container: 'body',
            html: true,
            title: function () {
                var time = Math.round(self.getModelValue('startTime')) + '';
                if (time > 60) {
                    var secs = Math.round(time % 60);
                    time = Math.round(time / 60) + ':' +  (secs < 10 ? '0' + secs : secs);
                }

                var html = "";
                html += "<small>" + time + "s</small>";
                html += "<span style='margin-left: 5px;'>" + self.getModelValue('name') + "</span>";
//                html += "<span class='glyphicon glyphicon-trash' style='float:right;'></span>";
                html += "<span class='thumbs glyphicon glyphicon-thumbs-down' dir='down' id='" + self.id + "' style='float:right; cursor:pointer; margin-right: 7px;'></span>";
                html += "<span class='thumbs glyphicon glyphicon-thumbs-up' dir='up' id='" + self.id + "' style='float:right; cursor:pointer; margin-right: 7px;'></span>"

                return html;
            },
            content: function () {
                var html = "";
                //get url of video... add param
                // var url = self.widget.adapter.videoUrl + '&annotationId=' + self.id;

                html += "<span class='clipmine-share-annotation' id='" + self.getModelValue('id') + "'>share on Facebook</span>";
                return html;
            }
        });

        var self = this;
        this.labelEl.click (function () {
            self.widget.adapter.seekTo(self.getModelValue('startTime'));
        });

        return this.labelEl;
    }

    this.isTag = function () {
        return this.annotation.type == 0;
    }

    this.init = function(a, videoDuration, timeLineWidth) {
        console.log(a);
        this.id = a.id;

        //render
        var factor = a.startTime / videoDuration;
        var position = factor * timeLineWidth;

        this.render(position);
    }

    return this.init(a, videoDuration, timeLineWidth);
}



//this.drawConnection = function () {
//    var sourceElement = this.el;
//    var targetElement = this.labelEl;
//
//    //draw from/to the centre, not the top left
//    //don't use .position()
//    //that will be relative to the parent div and not the page
//    var sourceX = sourceElement.offset().left + sourceElement.width() / 2;
//    var sourceY = sourceElement.offset().top + sourceElement.height() / 2;
//
//    var targetX = targetElement.offset().left + sourceElement.width() / 2;
//    var targetY = targetElement.offset().top + sourceElement.height() / 2;
//
//    var canvas = $('#drawing');
//
//    //you need to draw relative to the canvas not the page
//    var canvasOffsetX = canvas.offset().left;
//    var canvasOffsetY = canvas.offset().top;
//
//    var context = canvas[0].getContext('2d');
//
//    //draw line
//    context.beginPath();
//    context.moveTo(sourceX - canvasOffsetX, sourceY - canvasOffsetY);
//    context.lineTo(targetX - canvasOffsetX, targetY - canvasOffsetY);
//    context.closePath();
//    //ink line
//    context.lineWidth = 2;
//    context.strokeStyle = "#000"; //black
//    context.stroke();
//}