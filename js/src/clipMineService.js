//clipmine service singleton
var clipMineService = function () {
    function getEndpoint () {
        return widgetOptions.configType == 'dev' ? "http://localhost:8000" : "http://alpha.clipmineinc.com";
    }

    return {
        postToFacebook: function (userId, annotationId) {
            var data = {
                userId: userId,
                annotationId: annotationId
            }
            $.get(getEndpoint(), data, function (response) {
                if (response == 1) {
                    console.log('Successfully posted');
                }
            })
        },

        findVideoBySite: function (siteVideoInfo, _success, _error) {
            $.get(getEndpoint() + '/api/find_video_by_site', siteVideoInfo, function (data) {
                console.log(data)
                if (data.status == 1) {
                    _success(data.video, data.annotations);
                }
                else {
                    _error();
                }
            }, "json");
        },

        saveAnnotation: function (postData, _success, _error) {
            $.post(getEndpoint() + '/api/save_annotation', postData, function (data) {
                if (data.status == 1) {
                    if (_success)
                        _success(data.annotation);
                }
                else {
                    if (_error)
                        _error();
                }
            }, 'json')
        },

        displayOff: function (annotationId, el) {
            $.post(getEndpoint() + '/api/annotation_display_off', {'annotationId':annotationId}, function (data) {
                $(el).parent().remove();
            }, 'json')
        }

    }
}();
