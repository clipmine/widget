//clipmine widget - represents a clipmine widget
function ClipMineWidget (el, siteAdapter, options) {
    this.el = el;
    this.adapter = siteAdapter;
    this.video = null;
    this.annotations = [];
    this.searchData = [];
    this.comments = [];
    this.tagMode = true;
    this.searchMode = true;
    this.userId = null;
    this.sliderMinTime = 0;
    this.sliderMaxTime = 0;
    this.startTime = null;
    this.options = options;
    this.playStarted = false;

    this.totalTopSoFar = 0;
    this.totalBottomSoFar = 20;
    this.top = true;

    return this.init();
}

ClipMineWidget.prototype = {
    ready: function () {
        var self = this;
        function success (video, annotations) {
            self.video = video;

            for(var i = 0; i < annotations.length; i++) {
                self.registerVideoAnnotation(annotations[i]);
            }

            self.renderVideoAnnotations();
            self.adapter.playVideo();
            console.log(self.searchData);
        }

        function error () {

        }

        //load video
        clipMineService.findVideoBySite (this.adapter.getSiteVideoIdInfo(), success, error);
        $('#search-input').autocomplete( "option", "source", this.searchData );
    },
    
    playing: function () {
        if (!this.playStarted) {
            //initialization options
            if (this.options.startTime) {
                this.adapter.seekTo(this.options.startTime);
            }

            this.playStarted = true;
        }
    },

    filterAnnotations: function (field, searchList) {
        if (searchList instanceof Array == false)
            searchList = [searchList];

        for (var i = this.annotations.length-1 ; i>=0; i--) {
            if (searchList.indexOf(this.annotations[i].getModelValue(field)) > -1) {
                this.annotations[i].show();
            }
            else {
                this.annotations[i].hide();
            }
        }
    },

    clearSearch: function () {
        $('#search-input').val('');
        this.renderVideoAnnotations();
    },

    setupSearch: function () {
        var self = this;
        $('#search-input').autocomplete({
            source: self.searchData,
            response: function (event, ui) {
                if (ui.content.length > 0) {
                    var searchArr = ui.content.map(function (item, index, arr) {
                        return item.value;
                    });
                    self.filterAnnotations('name', searchArr);
                }
                else {
                    self.clearSearch();
                }
            },
            select: function ( event, ui ) {
                $('#clear-search').css('display', 'block');
                self.filterAnnotations('name', ui.item.value);
                return false;
            }
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            $(ul).addClass('search-result-container');
            return $( "<li class='search-result'>" )
                .append( "<a>" + item.value + "</a>" )
                .appendTo( ul );
        };
    },

    setupEvents: function () {
        var self = this;
        $('#clipmine-annotation-input').keypress(function (e) {
            if (e.which == 13) {
                var val = $('#clipmine-annotation-input').val();
                console.log('Attempting to add new annotation ' + val);
                self.addNewAnnotation(val);
                $('#clipmine-annotation-input').val('');
            }
        });

        $('#search-input').keyup(function (e) {
            if ($(this).val().length == 0)
                self.clearSearch();
        });

        $('#clear-search').click(function () {
            console.log('Clearing search...');
            self.clearSearch();
            $(this).css('display', 'none');
        });

//        $('.mode-btn').on('click', function () {
//            var selected = $(this).attr('mode');
//            if (selected === "tags") {
//                self.tagMode = true;
//                $('#clipmine-annotation-input').attr('placeholder', 'Enter new tag...')
//            }
//            else {
//                self.tagMode = false;
//                $('#clipmine-annotation-input').attr('placeholder', 'Enter new comment...')
//            }
//            $('.mode-btn').removeClass('active');
//            $(this).addClass('active');
//            self.renderVideoAnnotations();
//        });

        //        $('#clipmine-login-link').click(function () {
//            window.open('http://localhost:8000/', 'Clipmine login', {
//                height: 400,
//                width: 600
//            });
//            return false;
//        })

        $('#video-control-button').click(function () {
            var state = $(this).attr('state');
            console.log('Player state is ' + state);
            switch (state) {
                case 'paused': $(this).switchClass("glyphicon-play", "glyphicon-pause");
                                $(this).attr('state', 'playing');
                                self.adapter.playVideo();
                                break;
                case 'playing': $(this).switchClass("glyphicon-pause", "glyphicon-play");
                                $(this).attr('state', 'paused');
                                self.adapter.stopVideo();
                                break;
                default: break;
            }
        });

        this.setupPopupEvents();
    },

    setupPopupEvents: function () {
        var self = this;
        $(document).on( "click", ".thumbs", function() {
            var id = $(this).attr('id');
            var dir = $(this).attr('dir');
            for (var i=0; i < self.annotations.length; i++) {
                if (self.annotations[i].id == id) {
                    if (dir == 'up')
                        self.annotations[i].voteUp();
                    else
                        self.annotations[i].voteDown();
                }
            }
        });

        $(document).on("click", ".annotation-remove", function () {
            clipMineService.displayOff($(this).attr('annotation-id'), this);

        });

        $(document).on("click", ".clipmine-share-annotation", function() {
            clipMineService.postToFacebook(self.userId, $(this).attr('id'));
        });
    },

    setupInterface: function () {
        var self = this;
        $(widgetContainerDiv).addClass(this.adapter.widgetClass);
        $(annotationDetailDiv).css('width', this.adapter.playerWidth + 'px');
        $(annotationFullDiv).css('width', this.adapter.playerWidth + 'px');
        $('#clipmine-control-container').css('width', this.adapter.playerWidth + 'px');


//        $('#clipmine-magnifier').css('width', this.adapter.playerWidth + 'px');
//        $(annotationFullDiv).slider({ from: 1000, to: 100000, step: 500, smooth: true, round: 0, dimension: "&nbsp;$", skin: "plastic" });

//        $(annotationFullDiv).slider({
//            range: true,
//            min: 0,
//            max: 500,
//            slide: function( event, ui ) {
//                var newMinTime =  ui.values[0];
//                var newMaxTime =  ui.values[1];
//                $('#clipmine-detail-mintime').text(newMinTime);
//                $('#clipmine-detail-maxtime').text(newMaxTime);
//                self.showTopLabels(newMinTime, newMaxTime);
//                self.sliderMinTime = newMinTime;
//                self.sliderMaxTime = newMaxTime;
//            }
//        });
    },

    init: function () {
        var self = this;
        $(this.el).append(widgetOptions.widgetHTML);
        this.userId = widgetOptions.userId;

        if (this.userId) {
            $('#clipmine-annotation-input').toggle();
            $('#clipmine-login-message').toggle();
        }

        this.setupInterface();
        this.setupEvents();
        this.setupSearch();

        //toggle search mode/ input mode
        if (this.searchMode) {

        }
    },

    saveAnnotation : function (annotationData, success, error) {
        clipMineService.saveAnnotation(annotationData, success, error);
    },

    addNewAnnotation: function (name) {
        var self = this;
        //a.showLabel(this.top ? this.totalTopSoFar : this.totalBottomSoFar, this.top ? '15px' : '35px');
        function success (annotation) {
            self.showAnnotation(self.registerVideoAnnotation(annotation));
        }
        function error () {

        }

        var annotationData = {
            'name': name,
            'videoId': this.video.id,
            'userId': this.userId,
            'siteUserId': this.adapter.userId,
            'startTime': this.adapter.getCurrentPlayerTime(),
            'type': 0,
            'tempBottomDisplay': true,
            'votesUp': 0,
            'votesDown': 0
        };

        console.log('Adding new annotation.');
        console.log(annotationData);

        this.saveAnnotation(annotationData, success, error);
    },

    registerVideoAnnotation: function (annotation) {
        var a = new AnnotationView(annotation, this, this.adapter.getDuration(), this.adapter.timeLineWidth);

        if (this.searchData.indexOf(a.getModelValue('name')) == -1)
            this.searchData.push(a.getModelValue('name'));
        this.annotations.push(a);
        return a;
    },

    clear: function () {
        this.annotations = [];
        this.searchData = [];
        $('.marker').remove();
        $('.annotation-label').remove();
        $('.annotation-comment').remove();
        $('#video-control-button').attr('state', 'playing');
        $('#video-control-button').switchClass('glyphicon-play', 'glyphicon-pause');
    },

    showAnnotation: function (annotation) {
        annotation.show();
    },

    renderVideoAnnotations: function () {
        for(var i = 0; i < this.annotations.length; i++) {
            this.showAnnotation(this.annotations[i]);
        }

        //this.showTopLabels(0, this.sliderMaxTime);
    },

    showTopLabels: function (newSliderMin, newSliderMax) {
//        $('.annotation-label').css('display', 'none');
        if (!this.annotations.length)
            return;
        
        //sort labels by votes
//        this.annotations.sort(function (a, b) {
//            if (a.votesUp > b.votesUp)
//                return 1;
//            else
//                return -1;
//        });

//        var labels = [];
//        for (var i=0; i<this.annotations.length; i++) {
//            var a = this.annotations[i];
//            if (a) {
//                labels.push(a);
//            }
//            else
//                break;
//        }

//        {
//                    name: a.get('name'),
//                    startTime: a.get('startTime')
//                }

        this.annotations.sort(function (a, b) {
            if (a.startTime > b.startTime)
                return 1;
            else if (a.startTime < b.startTime)
                return -1;
            return 0;
        });

        var aIdx = 0;
        while (aIdx < this.annotations.length) {
            var a = this.annotations[aIdx++];
            if (a && a.get('startTime') > 0 && a.get('startTime') <= this.adapter.getDuration()) {
                a.showLabel(this.totalTopSoFar, '17px');
                    this.totalTopSoFar += a.labelEl.width() + 20;
            }
        }

//        var aIdx = 0;
//        while (aIdx < this.annotations.length) {
//            var a = this.annotations[aIdx++];
//            if (a && a.get('startTime') > 0 && a.get('startTime') <= this.adapter.getDuration()) {
//                a.showLabel(this.top ? this.totalTopSoFar : this.totalBottomSoFar, this.top ? '15px' : '35px');
//                if (this.top)
//                    this.totalTopSoFar += a.labelEl.width() + 20;
//                else
//                    this.totalBottomSoFar += a.labelEl.width() + 20;
//
//                this.top = !this.top;
//            }
//        }
    },

    getPixelPosition: function (secs) {
        var factor = secs / this.sliderMaxTime;
        return factor * this.adapter.playerWidth;
    },

    prettyTime: function (sec_num) {
//        var sec_num = parseInt(this, 10); // don't forget the second parm
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;
        return time;
    }
}


//readyOld: function () {
//        var self = this;
//        function success (videos) {
//            if (videos.length > 0) {
//                console.log('Found ' + videos.length + ' videos');
//                self.video = videos[0];
//
//                console.log('Video id is ' + self.video.id);
//                if (!self.video.embedCode) {
//                    self.video.embedCode = self.adapter.embedCode;
//                    self.video.save();
//                }
//
//                self.retrieveAnnotations();
//            }
//            else {
//                self.video = parseService.createNew('Video');
//                self.video.save({
//                    'url': self.adapter.videoUrl,
//                    'embedCode': self.adapter.embedCode,
//                    'duration': self.adapter.getDuration()
//                }, {
//                    success: function(v) {
//                        // Execute any logic that should take place after the object is saved.
//                        console.log('New object created with objectId: ' + v.id);
//                        self.retrieveAnnotations();
//                    },
//                    error: function(v, error) {
//                        // Execute any logic that should take place if the save fails.
//                        // error is a Parse.Error with an error code and description.
//                        console.log('Failed to create new object, with error code: ' + error.description);
//                    }
//                });
//            }
//        }
//        function error (error) {
//        }
//
//        $('#clipmine-detail-maxtime').text(this.prettyTime(this.adapter.getDuration()));
////        $(annotationFullDiv).slider("option", "max", this.adapter.getDuration());
////        $(annotationFullDiv).slider("option", "values", [ 0, this.adapter.getDuration() ]);
//        this.sliderMaxTime = this.adapter.getDuration();
//
//        //find or create the video
//        console.log(this.adapter.videoUrl);
//        parseService.findByField("Video", {field: 'url', value: this.adapter.videoUrl}, success, error);
//        $('#search-input').autocomplete( "option", "source", this.searchData );
//    },

//    retrieveAnnotations: function () {
//        //makes a call to server to load tags/comments
//        var self = this;
//        function success (annotations) {
//            console.log('Found ' + annotations.length + ' for video with id ' + self.adapter.videoUrl);
//
//            //add annotations to timeline
//            for(var i = 0; i < annotations.length; i++) {
//                self.registerVideoAnnotation(annotations[i]);
//            }
//
//            self.renderVideoAnnotations();
//            self.adapter.playVideo();
//        }
//        function error (error) {}
//
//        parseService.findByField("Annotation", {field: 'videoId', value: this.video.id}, success, error, 'startTime');
//    },
